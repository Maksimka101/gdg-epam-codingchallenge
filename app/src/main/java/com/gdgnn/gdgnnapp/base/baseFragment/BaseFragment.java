package com.gdgnn.gdgnnapp.base.baseFragment;

import android.os.Bundle;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.gdgnn.gdgnnapp.base.mvp.IPresenter;

import org.jetbrains.annotations.NotNull;

public abstract class BaseFragment<P extends BaseFragmentContract.Presenter> extends Fragment implements BaseFragmentContract.View {

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getPresenter().attachView(this);
        getPresenter().onViewCreated();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        getPresenter().detachView();
    }

    @NotNull
    protected abstract P getPresenter();
}
