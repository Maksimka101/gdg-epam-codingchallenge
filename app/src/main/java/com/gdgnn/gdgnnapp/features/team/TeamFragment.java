package com.gdgnn.gdgnnapp.features.team;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.gdgnn.gdgnnapp.R;
import com.gdgnn.gdgnnapp.base.baseFragment.BaseFragment;
import com.gdgnn.gdgnnapp.utils.RemoteApiHelper;

import org.jetbrains.annotations.NotNull;

public class TeamFragment extends BaseFragment<TeamContract.Presenter> implements TeamContract.View {

    private TeamContract.Presenter presenter = new TeamPresenter(this);

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_team, container, false);
    }

    @Override
    public void setText(String text) {
        final TextView textView = getView().findViewById(R.id.text_team);
        textView.setText(text);
    }

    @NotNull
    @Override
    protected TeamContract.Presenter getPresenter() {
        return presenter;
    }
}