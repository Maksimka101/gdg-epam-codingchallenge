package com.gdgnn.gdgnnapp.features.schedule;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.gdgnn.gdgnnapp.R;
import com.gdgnn.gdgnnapp.base.baseFragment.BaseFragment;

import org.jetbrains.annotations.NotNull;

public class ScheduleFragment extends BaseFragment<ScheduleContract.Presenter> implements ScheduleContract.View {

    private ScheduleContract.Presenter presenter = new SchedulePresenter(this);

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_schedule, container, false);
    }

    @Override
    public void setText(String text) {
        final TextView textView = getView().findViewById(R.id.text_schedule);
        textView.setText(text);
    }

    @NotNull
    @Override
    protected ScheduleContract.Presenter getPresenter() {
        return presenter;
    }
}