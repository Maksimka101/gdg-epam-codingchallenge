package com.gdgnn.gdgnnapp.features.send;

import com.gdgnn.gdgnnapp.base.baseFragment.BaseFragmentContract;

interface SendContract {
    interface View extends BaseFragmentContract.View {
        void setText(String text);
    }
    interface Presenter extends BaseFragmentContract.Presenter { }
}
