package com.gdgnn.gdgnnapp.features.team;

import com.gdgnn.gdgnnapp.base.baseFragment.BasePresenter;

public class TeamPresenter extends BasePresenter<TeamContract.View> implements TeamContract.Presenter {
    private TeamContract.View view;

    public TeamPresenter(TeamContract.View view) {
        super(view);
        this.view = view;
    }

    @Override
    public void onViewCreated() {
        view.setText("This is team fragment");
    }
}