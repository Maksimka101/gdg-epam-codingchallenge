package com.gdgnn.gdgnnapp.features.send;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.gdgnn.gdgnnapp.R;
import com.gdgnn.gdgnnapp.base.baseFragment.BaseFragment;

import org.jetbrains.annotations.NotNull;

public class SendFragment extends BaseFragment<SendContract.Presenter> implements SendContract.View {

    private SendContract.Presenter presenter = new SendPresenter(this);

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_send, container, false);
    }

    @Override
    public void setText(String text) {
        final TextView textView = getView().findViewById(R.id.text_send);
        textView.setText(text);
    }

    @NotNull
    @Override
    protected SendContract.Presenter getPresenter() {
        return presenter;
    }
}