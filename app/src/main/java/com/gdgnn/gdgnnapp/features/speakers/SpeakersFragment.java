package com.gdgnn.gdgnnapp.features.speakers;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.gdgnn.gdgnnapp.R;
import com.gdgnn.gdgnnapp.base.baseFragment.BaseFragment;

import org.jetbrains.annotations.NotNull;

public class SpeakersFragment extends BaseFragment<SpeakersContract.Presenter> implements SpeakersContract.View {

    private SpeakersContract.Presenter presenter = new SpeakersPresenter(this);

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_speakers, container, false);
    }

    @Override
    public void setText(String text) {
        final TextView textView = getView().findViewById(R.id.text_speakers);
        textView.setText(text);
    }

    @NotNull
    @Override
    protected SpeakersContract.Presenter getPresenter() {
        return presenter;
    }
}