package com.gdgnn.gdgnnapp.utils;

import android.content.Context;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

public final class PicassoHelper {
    public static void load(Context context, String url, ImageView into) {
        Picasso.with(context).load(url).into(into);
    }
}
