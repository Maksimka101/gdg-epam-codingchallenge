package com.gdgnn.gdgnnapp.utils;


import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitBuilder {

    private static Retrofit instance;
    private static final String BASE_URL = "http://myjson.com/";

    private RetrofitBuilder() {
    }

    public static Retrofit getInstance() {
        if (instance == null) {
            instance = new retrofit2.Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
        return instance;
    }
}
