package com.gdgnn.gdgnnapp.utils;

public final class RemoteApiHelper {
    public static RemoteApiService getRemoteApiService() {
        return RetrofitBuilder.getInstance().create(RemoteApiService.class);
    }
}
